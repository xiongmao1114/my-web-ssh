package com.mryan.webssh.pojo;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.JSch;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.socket.WebSocketSession;

/**
 * @Description: ssh连接信息
 * @Author: MRyan
 * @Date: 2021/1/28
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SSHConnectInfo {
    private WebSocketSession webSocketSession;
    private JSch jSch;
    private Channel channel;

}
