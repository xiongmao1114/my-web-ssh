package com.mryan.webssh.asciinema;

import lombok.Data;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Data
public class CastHeader {
    private int version=2;
    private int width=225;
    private int height=48;
    private long timestamp;
    private Map<String,String> env;

    public CastHeader(){
        timestamp=new Date().getTime();
        env=new HashMap<>();
        env.put("SHELL","/usr/bin/zsh");
        env.put("TERM","xterm-256color");
    }
}
