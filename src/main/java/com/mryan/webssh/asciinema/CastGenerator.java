package com.mryan.webssh.asciinema;



import cn.hutool.core.io.FileUtil;

import java.io.File;

public class CastGenerator {
    public static File touch(String filename){
        return FileUtil.touch(filename);
    }

    public static File write(String content,File file){
        return FileUtil.appendUtf8String(content,file);
    }

    public static void main(String[] args) {
        String filename="123.cast";
        File file=touch(filename);
        file=write("ok",file);
    }
}
