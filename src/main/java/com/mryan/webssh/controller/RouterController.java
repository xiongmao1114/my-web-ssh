package com.mryan.webssh.controller;

import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
public class RouterController {
    @Resource
    private SimpMessagingTemplate simpMessagingTemplate;

    @RequestMapping({"/","/login","/index"})
    public String websshpage() {
        return "webssh";
    }

    @RequestMapping("/push-msg")
    @ResponseBody
    public String sockPush(){
        this.simpMessagingTemplate.convertAndSend("/push","abc");
        return "msg";
    }
}
