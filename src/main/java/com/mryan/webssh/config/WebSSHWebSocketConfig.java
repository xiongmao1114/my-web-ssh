package com.mryan.webssh.config;


import com.mryan.webssh.interceptor.WebSocketInterceptor;
import com.mryan.webssh.websocket.WebSSHWebSocketHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.AbstractWebSocketMessage;
import org.springframework.web.socket.config.annotation.*;

/**
* @Description: websocket配置
* @Author: MRyan
* @Date: 2021/1/28
*/
@Configuration
@EnableWebSocket
@EnableWebSocketMessageBroker
public class WebSSHWebSocketConfig implements WebSocketConfigurer, WebSocketMessageBrokerConfigurer {

    @Autowired
    WebSSHWebSocketHandler webSSHWebSocketHandler;

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry webSocketHandlerRegistry) {
        //socket通道
        //指定处理器和路径
        webSocketHandlerRegistry.addHandler(webSSHWebSocketHandler, "/webssh")
                .addInterceptors(new WebSocketInterceptor())

                .setAllowedOrigins("*");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry stompEndpointRegistry) {
        // TODO Auto-generated method stub
        stompEndpointRegistry.addEndpoint("/my-websocket").setAllowedOriginPatterns("*");
        stompEndpointRegistry.addEndpoint("/my-websocket").setAllowedOriginPatterns("*").withSockJS();
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        // 配置接受订阅消息地址前缀为topic的消息

        config.enableSimpleBroker("/push");
        // Broker接收消息地址前缀
//        config.setApplicationDestinationPrefixes("/app");
    }
}
